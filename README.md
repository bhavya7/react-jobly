#### Make sure to **Fork** the repo and work on a **Seperate** branch

## Step Zero: Setup

**The backend for this will be our solution to the express-jobly exercise.**

You can find this in the starter code, use the `backend` folder. It is an express server

Re-create the jobly database from the backend solution using the jobly.sql file. For reference [use this](https://stackoverflow.com/questions/6842393/import-sql-dump-into-postgresql-database)

Create a new React project, such that the dir structure is:

```
React-Jobly:
    - backend
    - frontend(this is your new react app)
```

It may help to take a few minutes to look over the backend to remind yourself of the most important routes.

Start up the backend. We have it starting on port 3001, so you can run the React front end on 3000.

<hr>

## Step One: Design Component Hierarchy

It will help you first get a sense of how this app should work.

We have a demo running at https://lethal-comparison.surge.sh/. Take a tour and note the features.

**Note: Ignore the Login/signup for now, we'll cover it later**

You can register as a new user and explore the site.

A big skill in learning React is to learn to design component hierarchies.

It can be very helpful to sketch out a hierarchy of components, especially for larger apps, like Jobly.

As an example of this kind of diagram, here’s one for a sample todo list application:
![Image](images/todoDiagram.png)

Once you’ve done this, it’s useful to think about the props and state each component will need. Deciding where individual state is needed is one of the most critical things to figure out.

Here’s our simple todo list application, with component state and passed props:
![Image](images/todoDetailDiagram.png)

We’re showing these to you with diagrams, and it can be helpful to do this with pen and paper or using a whiteboard.

You can also write this out as an indented list:

```
App                          General page wrapper
  no props or stage

  TodoList                   Manages todos, shows form &
    state=todos                list of todos

    NewTodoForm              Manages form data, submits
      state=formData           new todo to parent
      props=addTodo()

    Todo                     One rendered for each todo,
      props=title, descrip     pure presentational

```

Take time to diagram what components you think you’ll need in this application, and what the most important parts of state are, and where they might live.

Notice how some things are common: the appearance of a job on the company detail page is the same as on the jobs page. You should be able to re-use that component.

Spend time here. This may be one of the most important parts of this exercise.

<hr>

## Step Two: Make an API Helper

Many of the components will need to talk to the backend (the company detail page will need to load data about the company, for example).

It will be messy and hard to debug if these components all had AJAX calls buried inside of them.

Instead, make a single JoblyAPI class, which will have helper methods for centralizing this information. This is conceptually similar to having a model class to interact with the database, instead of having SQL scattered all over your routes.

Here’s a starting point for this file:

api.js

```
import axios from "axios";

const BASE_URL = process.env.REACT_APP_BASE_URL || "http://localhost:3001";

/** API Class.
 *
 * Static class tying together methods used to get/send to to the API.
 * There shouldn't be any frontend-specific stuff here, and there shouldn't
 * be any API-aware stuff elsewhere in the frontend.
 *
 */

class JoblyApi {
  // the token for interactive with the API will be stored here.
  static token;

  static async request(endpoint, data = {}, method = "get") {
    console.debug("API Call:", endpoint, data, method);

    //there are multiple ways to pass an authorization token, this is how you pass it in the header.
    //this has been provided to show you another way to pass the token. you are only expected to read this code for this project.
    const url = `${BASE_URL}/${endpoint}`;
    const headers = { Authorization: `Bearer ${JoblyApi.token}` };
    const params = (method === "get")
        ? data
        : {};

    try {
      return (await axios({ url, method, data, params, headers })).data;
    } catch (err) {
      console.error("API Error:", err.response);
      let message = err.response.data.error.message;
      throw Array.isArray(message) ? message : [message];
    }
  }

  // Individual API routes

  /** Get details on a company by handle. */

  static async getCompany(handle) {
    let res = await this.request(`companies/${handle}`);
    return res.company;
  }

  // obviously, you'll add a lot here ...
}

// for now, put token ("testuser" / "password" on class)
JoblyApi.token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZ" +
    "SI6InRlc3R1c2VyIiwiaXNBZG1pbiI6ZmFsc2UsImlhdCI6MTU5ODE1OTI1OX0." +
    "FtrMwBQwe6Ue-glIFgz_Nf8XxRT2YecFCiSpYL0fCXc";

```

You won’t build authentication into the front end for a while—but the backend needs a token to make almost all API calls. Therefore, for now, we’ve hard-coded a token in here for the user “testuser”, who is also in the sample data.

(Later, once you start working on the login form, you may find it useful to log in as “testuser”. Their password is “password”).

You can see a sample API call — to **_getCompany(handle)_**. As you work on features in the front end that need to use backend APIs, add to this class.

<hr>

## Step Three: Make Your Routes File

Look at the working demo to see the routes you’ll need:

```
/
Homepage — just a simple welcome message
/companies
List all companies
/companies/apple
View details of this company
/jobs
List all jobs
/login
Login/signup
/signup
Signup form
/profile
Edit profile page
```

Make your routes file that allows you to navigate a skeleton of your site. Make simple placeholder components for each of the feature areas.

Make a navigation component to be the top-of-window navigation bar, linking to these different sections.

When you work on authentication later, you need to add more things here. But for now, you should be able to browse around the site and see your placeholder components.

<hr>

## Step Four: Companies & Company Detail

Flesh out your components for showing detail on a company, showing the list of all companies, and showing simple info about a company on the list (we called these **_CompanyDetail_**, **_CompanyList_**, and **_CompanyCard_**, respectively —but you might have used different names).

Make your companies list have a search box, which filters companies to those matching the search (remember: there’s a backend endpoint for this!). Do this filtering in the backend — **not** by loading all companies and filtering in the front end!

<hr>

## Step Five: Jobs

Similarly, flesh out the page that lists all jobs, and the “job card”, which shows info on a single job. You can use this component on both the list-all-jobs page as well as the show-detail-on-a-company page.

Don’t worry about the “apply” button for now — you’ll add that later, when there’s authentication for the app.
